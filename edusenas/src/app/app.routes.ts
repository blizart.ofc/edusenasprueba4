import { Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegistrerComponent } from './registrer/registrer.component';
import { MenuComponent } from './menu/menu.component';
import { CamaraComponent } from './menu/camara/camara.component';
import { VideojuegoOneComponent } from './videojuego-one/videojuego-one.component';
import { JuegosComponent } from './juegos/juegos.component';
import { MemoramaComponent } from './memorama/memorama.component';
import { AbecedarioComponent } from './abecedario/abecedario.component';
import { FamiliaComponent } from './familia/familia.component';
import { CuestionarioComponent } from './cuestionario/cuestionario.component';
import { ColoresComponent } from './colores/colores.component';
import { MasComponent } from './mas/mas.component';
import { PerfilComponent } from './perfil/perfil.component';
import { DiccionarioComponent } from './diccionario/diccionario.component';
import { AdivinalasenaComponent } from './adivinalasena/adivinalasena.component';
import { VideojuegosSaltarComponent } from './videojuegos-saltar/videojuegos-saltar.component';


export const routes: Routes = [
    {path:'',component:HomeComponent},
    {path:'home', component:HomeComponent},
    {path:'camera', component:CamaraComponent},
    {path:'login', component:LoginComponent},
    {path: 'registrer', component:RegistrerComponent},
    //menu botones izq
    {path:'menu', component:MenuComponent},
    {path:'diccionario', component:DiccionarioComponent},
    {path: 'juegos', component:JuegosComponent},
    {path:'perfil',component:PerfilComponent},
    {path:'mas',component:MasComponent},
    //menu botones der
    {path: 'abecedario', component:AbecedarioComponent},
    {path:'familia', component:FamiliaComponent},
    {path:'cuestionario', component:CuestionarioComponent},
    {path:'colores',component:ColoresComponent},
    //videojuegos
    {path: 'rompecabezas', component:VideojuegoOneComponent},
    {path: 'memorama', component:MemoramaComponent},
    {path: 'adivinalasena', component:AdivinalasenaComponent},
    {path: 'salto', component:VideojuegosSaltarComponent}
];
